# Fan thermostat #

PCB design in Kicad5 for temperature based pc fan control.

### Features ###

* Input voltage 6-24V dc
* Maximal current 3A
* Turn on temperature can be set in range 16,5°C - 81,5°C
* Turn off tempereture can not be set and equals to 10°C tempretature drop

### Schematic ###
![Schematic](fan_thermostat_kicad5-schematic.png "Schematic")

### PCB ###
![PCB](fan_thermostat_kicad5.png "3D PCB")

Version 1.0
